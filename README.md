# Nombre de la activadad: Matemáticas
## Mapa conceptual 1: Conjuntos, Aplicaciones y funciones (2002)

```plantuml
@startmindmap
*[#FF4500] Conjuntos
	*_ son
		*[#F08080] Ideas intuitivas asumidas por el observador
	*_ y
		*[#F08080] Elementos 
			*_ Tienen
				*[#00BFFF] Relación no definida
	*_ Complementos
		*[#F08080] Inclusión de Conjunto y Cardinal de Conjuntos
			*_ Se refiere a que 
				*[#00BFFF] Todos los elementos del primero pertenece al segundo
				*[#00BFFF] No. de elementos que conforman el conjunto
		*[#F08080] Actuación de ordinales
			*_ Son
				*[#00BFFF] Relaciones de quien es >,<,o = entre cardinales.
	*_ Algunas operaciones
		*[#F08080] Interceptición
			*_ se da
				*[#00BFFF] Cuando los elementos pertenecen simultanemanete a ámbos
		*[#F08080] Unión
			*[#00BFFF] Pertenecen al menos a uno de ellos
		*[#F08080] Complementos
			*_ Son
				*[#00BFFF] Los que no pertenecen completam de un conjunto\n que no pertenece a un conjunto dado
	*_ Tipos
		*[#F08080] Inyectiva
		*[#F08080] Subjetiva
		*[#F08080] Biyectiva
	*_ Ejemplos
		*[#F08080] Universal
		*[#F08080] Vacío
@endmindmap
```
### Vídeos de referencia
[Conjuntos, Aplicaciones y funciones (2002)](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)


## Mapa Conceptual 2: Funciones (2010)

```plantuml
@startmindmap
*[#FF4500] Funciones 
	*_ Definición
		*[#F08080] Transformación de un conjunto con otro conjunto
			*_ ejemplos característicos
				*[#00BFFF] El cambio del valor de las monedas
				*[#00BFFF] Cambio de la temperatura
		*[#F08080] Regla de correspondencia entre dos conjuntos
			*_ se refiere 
				*[#00BFFF] A que cada elemento del primer conjutno le corresponde\n uno y solo un elemento del segundo conjunto
			*[#00BFFF] Conjunto de núneros reales
	*_ Tienen
		*[#F08080] Representaicón gráfica
			*_ Ejemplo
				*[#00BFFF] Representación Cartesiana
					*[#9ACD32] Los ejes de números reales pueden\nser representados en una recta
					*_ Ejes de las 
						*[#9ACD32] X
						*[#9ACD32] Y
	*_ Tipos de funciones
		*[#F08080] Funciones crecientes
			*[#00BFFF] Aumento de la variable independiente
				*[#9ACD32] Al aumentar las X aumentan sus imágenes
		*[#F08080] Funciones decrecientes
			*[#00BFFF] Al disminuir las variables las imágenes lo hacen de igual forma
	*_ Características de\nlas funciones
		*[#F08080] Máximo
			*_ donde
				*[#00BFFF] La función ha alcanzado el valor máximo
		*[#F08080] Mínimo
			*_ donde
				*[#00BFFF] La función ha alcnazado el valor mínimo
		*[#F08080] Límite
			*_ donde
				*[#00BFFF] Aproximarse al punto de interés
		*[#F08080] Intervalo
			*_ es
				*[#00BFFF] Magnitud entre dos límites dados que toman los conjuntos de valores
		*[#F08080] Continuidad
			*_ es
				*[#00BFFF] Una línea no interrumpida
		*[#F08080] Deribada
			*_ es
				*[#00BFFF] La derivada es la que mide el cambio, la aproximación\nlocal de una función
			*_ nos ayuda
				*[#00BFFF] Nos ayuda a resolver el problema de la aproximación de una\nfunción compleja mediante una función lineal
			*_ existen algunas relgas
				*[#00BFFF] Derivada de una función
				*[#00BFFF] Derivada de un producto
				*[#00BFFF] Derivada de una potencia

			*_ Aplicaciones donde se encuentra
				*[#00BFFF] Economía
				*[#00BFFF] Ciencias Sociales
				*[#00BFFF] Ingeniería
@endmindmap
```
### Vídeos de referencia
[Funcioones (2010)](https://canal.uned.es/video/5a6f2d09b1111f21778b457f)


## Mapa conceptual 3: La matemática del computador (2002)

```plantuml
@startmindmap
*[#FF4500] Aritmética de la computadora
	*_ Las matemáticas
		*[#F08080] Es la ciencia básica detrás de todo
		*[#F08080] Representaciones de números muy grandes con exponente 10
			*_ El trabajar con números finitos\n de decimales hay problemas como
				*[#00BFFF] Número aproximado
				*[#00BFFF] Idea de error
				*[#00BFFF] Digitos significativos
	*_ Algunos conceptos
		*[#F08080] Truncar
			*[#00BFFF] Cortar los decimales, unos cuantos números a la derecha
		*[#F08080] Redondear
			*[#00BFFF] Truncamiento refinado, despreciar y retocar la última cifra
				*_ esto
					*[#9ACD32] Para evitar el menor error
		*[#F08080] Codificación
			*[#00BFFF] Representación de números en el ordenador
	*[#F08080] Sistemas de números
		*_ Encontramos
			*[#00BFFF] Sistema Binario
				*_ es el
					*[#9ACD32] Sistema de la computadora
						*_ Conformado por
							*[#20B2AA] 0 y 1
					*[#9ACD32] Encendido y apagado
				*[#9ACD32] Se pueden realizar operaciones 
					*_ Como 
						*[#20B2AA] Sumas
				*_ Representación de 
					*[#9ACD32] Vídeos
					*[#9ACD32] Imágenes
				*_ Aplicaciones
					*[#9ACD32] Circuitos
					*[#9ACD32] Encendido o apagado 
						*[#20B2AA] Impulso de corriente
						*[#20B2AA] Presencia o ausencia de corriente.
			*[#00BFFF] Sistema Octal
				*[#9ACD32] base 8
			*[#00BFFF] Sistema Hexadecimal
				*[#9ACD32] Base 16
			*[#00BFFF] Aritmética de Punto flotante
				*[#9ACD32] La operaciones como sumas, multiplicaciones.
					*_ se busca
						*[#20B2AA] Como representar en una notación científica
			*[#00BFFF] Desbordamiento
@endmindmap
```
### Vídeos de referencia
1. [La matemática del computador (2002)](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa)
2. [La matemática del computador (2002)](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)

## Autor: Vásquez Alcántara Roberto Alejandro 

[VasquezAlcantaraRobertoAlejandro @ GitLab](https://gitlab.com/VasquezAlcantaraRobertoAlejandro)
